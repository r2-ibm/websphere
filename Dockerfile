FROM node:18-buster as build
WORKDIR /app
COPY package*.json /app/
RUN yarn install
COPY ./ /app/
RUN yarn run build

FROM nginx:1.23
COPY --from=build /app/build/ /usr/share/nginx/html
